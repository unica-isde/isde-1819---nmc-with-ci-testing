import numpy as np
from matplotlib import pyplot as plt
from fun_utils import split_data, plot_ten_images, compute_ts_error
from classifiers.nmc import NMC
from data_loaders import CDataLoaderMNIST
from data_perturb import CDataPerturbGaussian, CDataPerturbRandom


def compute_ts_accuracy(ypred, yts):
    """
    Compute the fraction of elements that are equal in ypred and yts
    (classification accuracy)

    Parameters
    ----------
    ypred: the set of predicted class labels
    yts: the true labels of test samples

    Returns
    -------
    test_accuracy: the classification accuracy
    """
    test_accuracy = np.sum(ypred == yts) / float(ypred.size)
    print "Test Accuracy (" "on ", yts.size, " test samples): ", test_accuracy
    return test_accuracy


# load
filename = '../data/mnist_data.csv'  # the location of the CSV file
data_loader = CDataLoaderMNIST(filename)
X, y = data_loader.load_data()
w = data_loader.w
h = data_loader.h

# split
Xtr, ytr, Xts, yts = split_data(X, y, tr_fraction=0.6)

# get class labels
classes = np.unique(ytr)

# perturb Xts with default parameters
perturb_gaussian = CDataPerturbGaussian()
perturb_random = CDataPerturbRandom()
Xp_gauss = perturb_gaussian.perturb_dataset(Xts)
Xp_rand = perturb_random.perturb_dataset(Xts)

# display images
titles = ['' for i in xrange(10)]
plot_ten_images(Xts, w, h, titles)
plt.show()
plot_ten_images(Xp_gauss, w, h, titles)
plt.show()
plot_ten_images(Xp_rand, w, h, titles)
plt.show()

# train the classifier
clf = NMC()
clf.fit(Xtr, ytr)

classes = np.unique(ytr)  # list of training class labels
titles = ['y: ' + str(i) for i in classes]
plot_ten_images(clf.centroids, w, h, titles)
plt.show()

yc = clf.predict(Xts)
test_accuracy = compute_ts_accuracy(yc, yts)

# compute test accuracy vs k for random noise
k_values = [0, 10, 20, 50, 100, 200, 500]
test_accuracy_rand = np.zeros(len(k_values))
for iter, k in enumerate(k_values):
    perturb_random.K = k  # set value of K
    X_perturbed = perturb_random.perturb_dataset(Xts)
    ypred = clf.predict(X_perturbed)
    test_accuracy_rand[iter] = compute_ts_accuracy(ypred, yts)

# compute test accuracy vs sigma for Gaussian noise
sigma_values = [10, 20, 100, 200, 500]
test_accuracy_gaussian = np.zeros(len(sigma_values))
for iter, sigma in enumerate(sigma_values):
    perturb_gaussian.sigma = sigma  # set value of sigma
    X_perturbed = perturb_gaussian.perturb_dataset(Xts)
    ypred = clf.predict(X_perturbed)
    test_accuracy_gaussian[iter] = compute_ts_accuracy(ypred, yts)

plt.subplot(1, 2, 1)
plt.plot(k_values, test_accuracy_rand)
plt.xlabel('K')
plt.title('Test accuracy vs K')

plt.subplot(1, 2, 2)
plt.plot(sigma_values, test_accuracy_gaussian)
plt.xlabel('sigma')
plt.title('Test accuracy vs sigma')
plt.show()
