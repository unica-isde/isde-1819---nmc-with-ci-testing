from abc import ABCMeta, abstractmethod
import numpy as np


class CDataPerturb(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def data_perturbation(self, x):
        raise NotImplementedError("data_perturbation not implemented!")

    def perturb_dataset(self, X):
        Xp = np.zeros(X.shape)
        for i in xrange(X.shape[0]):
            Xp[i, :] = self.data_perturbation(X[i, :])
        return Xp
