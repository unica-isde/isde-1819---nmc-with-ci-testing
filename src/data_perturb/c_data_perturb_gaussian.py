from c_data_perturb import CDataPerturb
from copy import deepcopy
import numpy as np
import warnings


class CDataPerturbGaussian(CDataPerturb):

    def __init__(self, min_value=0, max_value=255, sigma=100.0):
        self._sigma = None  # creating empty "protected" members
        self._min_value = None
        self._max_value = None

        self.sigma = sigma  # calling setters
        self.min_value = min_value
        self.max_value = max_value

    @property
    def sigma(self):
        return self._sigma

    @sigma.setter
    def sigma(self, value):
        self._sigma = float(value)

    @property
    def min_value(self):
        return self._min_value

    @property
    def max_value(self):
        return self._max_value

    @min_value.setter
    def min_value(self, value):
        self._min_value = value

    @max_value.setter
    def max_value(self, value):
        self._max_value = value

    def data_perturbation(self, x):
        """
        Perturb an image with Gaussian noise N(0, sigma^2)
        """
        xp = np.array(deepcopy(x), dtype=float)  # vector elements to float
        xp += self._sigma * np.random.randn(x.shape[0])
        xp[xp < self._min_value] = self._min_value
        xp[xp > self._max_value] = self._max_value
        xp = xp.astype(dtype=int)  # vector elements back to integer
        return xp
