from c_data_perturb import CDataPerturb
from copy import deepcopy
import numpy as np
import warnings


class CDataPerturbRandom(CDataPerturb):

    def __init__(self, min_value=0, max_value=255, K=100):
        self._min_value = None
        self._max_value = None
        self._K = None

        self.min_value = min_value
        self.max_value = max_value
        self.K = K

    @property
    def min_value(self):
        return self._min_value

    @property
    def max_value(self):
        return self._max_value

    @property
    def K(self):
        return self._K

    @min_value.setter
    def min_value(self, value):
        self._min_value = value

    @max_value.setter
    def max_value(self, value):
        self._max_value = value

    @K.setter
    def K(self, value):
        self._K = int(value)

    def data_perturbation(self, x):
        """
         Perturb an image with random noise

         Parameters
         ----------
         x: the image to be perturbed
         k: the number of pixels to be arbitrarily changed

         Returns
         -------
         xp: the perturbed image

         """

        if self._K > x.size:
            warnings.warn(
                'The number of elements to be modified is larger than x. '
                'Modifying all values.')
            k = x.size
        else:
            k = self._K

        xp = deepcopy(x)
        # create a vector containing the index of each pixel and shuffle them
        idx_pixels = np.array(range(0, xp.size))
        np.random.shuffle(idx_pixels)
        # then, take the first k elements (the pixels to be perturbed)
        idx_pixels = idx_pixels[0:k]
        # generate k integers at random in {0,1, ..., 255} and assign them
        # to be pixels selected to be perturbed
        xp[idx_pixels] = np.random.randint(self._min_value,
                                           self._max_value + 1,
                                           size=(k,))
        return xp
