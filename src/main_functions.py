import numpy as np
from matplotlib import pyplot as plt
from fun_utils import count_samples_per_class, \
    split_data, plot_ten_images, compute_ts_error, fit, predict
from data_loaders import CDataLoaderMNIST

filename = '../data/mnist_data.csv'  # the location of the CSV file
data_loader = CDataLoaderMNIST(filename)
X, y = data_loader.load_data()
w = data_loader.w
h = data_loader.h

# dataset = fetch_lfw_people(min_faces_per_person=10)
# n_samples, h, w = dataset.images.shape
# X = dataset.data
# y = dataset.target

classes = np.unique(y)  # these may be non-consecutive indices...
print classes

# show how many digit samples we have in each class
print "Number of samples per class: ", count_samples_per_class(y)

num_samples = np.sum(count_samples_per_class(y))
num_samples = int(num_samples)  # cast to integer
print "Total number of samples: ", num_samples  # verify that they are 2890

# the total number of samples *must* be equal to the number of rows of X
assert (num_samples == X.shape[0])

Xtr, ytr, Xts, yts = split_data(X, y, tr_fraction=0.6)

print count_samples_per_class(ytr)

centroids = fit(Xtr, ytr)

# generate the list of titles (10 strings)
titles = ['y:' + str(classes[i]) for i in xrange(10)]
# create a plot with 2x5 subplots
plot_ten_images(centroids, w, h, titles)
plt.show()

# Next step: predicting the class label of unseen samples
ypred = predict(Xts, centroids, classes)

# let's compare the predictions and true labels on the first 10 samples in Xts
print ypred[0:10], yts[0:10]

# let's check which ones are different
# (preliminary step to compute the test error)
print ypred[0:10] != yts[0:10]

# let's now *count* how many predicted labels are different
# from the true labels, divided by the total number of labels.
# The fraction of such wrongly-predicted labels is the test error.
test_error = compute_ts_error(ypred, yts)

# list comprehension equivalent to the for loop commented below
titles = ["y: " + str(yts[i]) + " yp: " + str(ypred[i]) for i in xrange(10)]
# for i in xrange(10):
#    titles.append("y: " + str(yts[i]) + " yp: " + str(ypred[i]))

plot_ten_images(Xts, w, h, titles)
plt.show()
