import numpy as np
from matplotlib import pyplot as plt
from fun_utils import plot_image, split_data, plot_ten_images, compute_ts_error
from classifiers.nmc import NMC
from data_loaders import CDataLoaderMNIST
from copy import deepcopy


def perturb_image(x, k):
    """
    Perturb an image with random noise

    Parameters
    ----------
    x: the image to be perturbed
    k: the number of pixels to be arbitrarily changed

    Returns
    -------
    xp: the perturbed image

    """
    xp = deepcopy(x)
    # create a vector containing the index of each pixel and shuffle them
    idx_pixels = np.array(range(0, xp.size))
    np.random.shuffle(idx_pixels)
    # then, take the first k elements (which are the pixels to be perturbed)
    idx_pixels = idx_pixels[0:k]
    # generate k integer values at random in {0,1, ..., 255} and assign them
    # to be pixels selected to be perturbed
    xp[idx_pixels] = np.random.randint(0, 256, size=(k,))
    return xp


# this is a simple perturb test. Just perturbing a black image of 784 pixels
xp = perturb_image(np.zeros(shape=(784,)), 100)
plot_image(xp, 'perturbed')
plt.show()

# load / split data
filename = '../data/mnist_data.csv'  # the location of the CSV file
data_loader = CDataLoaderMNIST(filename)
X, y = data_loader.load_data()
w = data_loader.w
h = data_loader.h

# dataset = fetch_lfw_people(min_faces_per_person=10)
# n_samples, h, w = dataset.images.shape
# X = dataset.data
# y = dataset.target

Xtr, ytr, Xts, yts = split_data(X, y, tr_fraction=0.6)

# Now let's create the classifier.
clf = NMC()
clf.fit(Xtr, ytr)

# display the first ten centroids
classes = np.unique(ytr)  # list of training class labels
titles = ['y: ' + str(i) for i in classes]
plot_ten_images(clf.centroids, w, h, titles)
plt.show()

# classify the test samples (predict their labels), and compute the error
yc = clf.predict(Xts)
test_error = compute_ts_error(yc, yts)

# compute test error vs k
k_values = [0, 10, 20, 50, 100, 200, 500]
test_error = np.zeros(len(k_values))
for iter, k in enumerate(k_values):
    X_perturbed = np.zeros(Xts.shape)
    for i in xrange(Xts.shape[0]):
        X_perturbed[i, :] = perturb_image(Xts[i, :], k)
    ypred = clf.predict(X_perturbed)
    test_error[iter] = compute_ts_error(ypred, yts)

plt.plot(k_values, test_error)
plt.show()

titles = ['' for i in xrange(10)]
plot_ten_images(X_perturbed, w, h, titles)
plt.show()
