import numpy as np
from matplotlib import pyplot as plt
from fun_utils import split_data, plot_ten_images, compute_ts_error
from classifiers.nmc import NMC
from data_loaders import CDataLoaderMNIST

filename = '../data/mnist_data.csv'  # the location of the CSV file
data_loader = CDataLoaderMNIST(filename)
X, y = data_loader.load_data()
w = data_loader.w
h = data_loader.h

# dataset = fetch_lfw_people(min_faces_per_person=10)
# n_samples, h, w = dataset.images.shape
# X = dataset.data
# y = dataset.target

Xtr, ytr, Xts, yts = split_data(X, y, tr_fraction=0.6)

# data has been loaded and split. Now let's create the classifier.
clf = NMC()
clf.fit(Xtr, ytr)

classes = np.unique(ytr)  # list of training class labels
titles = ['y: ' + str(i) for i in classes]
plot_ten_images(clf.centroids, w, h, titles)
plt.show()

yc = clf.predict(Xts)
test_error = compute_ts_error(yc, yts)
