from c_data_loader import CDataLoader
import numpy as np
from pandas import read_csv


class CDataLoaderMNIST(CDataLoader):

    def __init__(self, filename=None):
        self._w = 28  # the image size is always the same in this data set
        self._h = 28
        self._filename = filename
        pass

    # w and h are read-only attributes
    @property
    def w(self):
        return self._w

    @property
    def h(self):
        return self._h

    # filename can be read and written, instead (set/get)
    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, value):
        self._filename = value

    def load_data(self):
        """
        Load data from a csv file

        Parameters
        ----------
        filename : string
            Filename to be loaded.

        Returns
        -------
        X : ndarray
            the data matrix.

        y : ndarray
            the labels of each sample.
        """
        if self._filename is None:
            raise ValueError(
                'Filename has not been set! Unable to load MNIST data')

        data = read_csv(self._filename)
        z = np.array(data)
        y = z[:, 0]
        X = z[:, 1:]
        return X, y
