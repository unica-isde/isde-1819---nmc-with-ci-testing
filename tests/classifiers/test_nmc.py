import unittest
import numpy as np
from src.classifiers import NMC


class TestNMC(unittest.TestCase):

    def _generate_data(self, n_classes=3, n_features=5, n_samples_class=5):
        # generate n_classes distinct labels
        class_labels = np.random.choice(range(0, 100), n_classes)
        # print "class labels: ", class_labels

        self.x_tr = np.zeros((n_classes * n_samples_class, n_features))
        self.y_tr = np.zeros((n_classes * n_samples_class,))

        for i in xrange(n_classes):
            idx_start = i * n_samples_class
            idx_end = idx_start + n_samples_class
            self.x_tr[idx_start: idx_end, :] = class_labels[i]
            self.y_tr[idx_start: idx_end] = class_labels[i]

        # print "y: ", self.y_tr
        # print "x: ", self.x_tr

    def _fit_classifier(self):
        self.nmc = NMC()
        self.nmc.fit(self.x_tr, self.y_tr)

    def setUp(self):
        self._generate_data()
        self._fit_classifier()

    def test_fit(self):
        """
        This test generates a set of labels and sets all feature values of x to
        the corresponding class label value. For example, if y=1, all x in this
        class will be [1 1 1 .... 1]. The corresponding centroid will thus be
        again equal to [1 1 1 .... 1]. This test thus checks if the centroids
        computed by fit are equivalent to the unique rows of the matrix x_tr.
        """
        self.assertTrue(
            (np.unique(self.x_tr, axis=0) == self.nmc.centroids).all())

    def test_predict(self):
        ypred = self.nmc.predict(self.nmc.centroids)
        # print ypred, self.nmc.class_labels
        self.assertTrue(
            (ypred == self.nmc.class_labels).all())
